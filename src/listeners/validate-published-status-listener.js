import {DataError, LangUtils} from '@themost/common';
const parseBoolean = LangUtils.parseBoolean;
/**
 * @param {import('@themost/data').DataEventArgs} event
 */
async function afterSaveAsync(event) {
    if (Object.prototype.hasOwnProperty.call(event.target, 'published')) {
        const published = parseBoolean(event.target.published);
        if (published === true) {
            // get previous status
            const previousPublished = parseBoolean(event.previous && event.previous.published);
            if (previousPublished === false) {
                /**
                 * @type {import('@themost/data').DataContext}
                 */
                const context = event.model.context;
                // get sign status
                const signed = await event.model.asQueryable().where('id').equal(event.target.id).select('signed').value();
                if (signed) {
                    return;
                }
                // search if the current document has been derived from a report template
                // which indicates that digital signing is required
                const item = await context.model('RequestDocumentActions')
                    .where('result').equal(event.target.id)
                    .select(
                        'id',
                        'object/reportTemplate/signReport as shouldSign'
                        ).silent().getItem();
                if (item && item.shouldSign) {
                    throw new DataError('E_SHOULD_SIGN', 'The selected document should be signed before publishing.', null, event.model.name, 'signed')
                }
            }
        }
    }
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return afterSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}