/* eslint-disable no-unused-vars */
import {DocumentNumberService} from './DocumentNumberService';
import DocumentNumberSeriesItem from './models/DocumentNumberSeriesItem';
import path from 'path';
import {TraceUtils, Args, DataError} from '@themost/common';
import {DataContext, DataModel} from '@themost/data';
import {HttpContext} from '@themost/web';
/**
 * @class
 * The default application service for managing document numbers and document series
 */
class DefaultDocumentNumberService extends DocumentNumberService {
    constructor(app) {
        super(app);
    }

    /**
     * @param {DataContext | HttpContext} context
     * @param {DocumentNumberSeries|*} documentSeries
     * @param {*=} extraAttributes
     * @returns 
     */
    async next(context, documentSeries, extraAttributes) {
        // validate id
        Args.notNumber(documentSeries.id, 'Item identifier');
        // get id
        const id = documentSeries.id;
        // get number format if empty
        const model = context.model('DocumentNumberSeries');
        let lastIndex;
        await new Promise((resolve, reject) => {
            context.db.executeInTransaction( (cb) => {
                // execute async method
               (async function() {
                   /**
                    * @type {{lastIndex: any, useParentIndex: boolean, parent: any, parentLastIndex: any}}
                    */
                   const item = await model.where('id').equal(id)
                        .select('lastIndex', 'useParentIndex', 
                        'parent/id as parent',
                        'parent/lastIndex as parentLastIndex').silent().getItem();
                   if (item.useParentIndex) {
                       // validate if parent is null
                       if (item.parent == null) {
                           // and throw exception
                           throw new DataError('E_FOUND', 'Parent document number series item cannot be found or is inaccessible', null, 'DocumentNumberSeries', 'parent');
                       }
                       // get parent last index
                       lastIndex = item.parentLastIndex;
                   } else {
                       // otherwise, use item lastIndex
                    lastIndex = item.lastIndex;
                   }
                   // increase last index
                   lastIndex += 1;
                   // and save
                   if (item.useParentIndex) {
                       // update parent.lastIndex and continue
                       await model.silent().save({
                           id: item.parent,
                           lastIndex: lastIndex
                       });
                   } else {
                       // update lastIndex
                       await model.silent().save({
                           id,
                           lastIndex
                       });
                   }
               })().then( () => {
                    return cb();
               }).catch( err => {
                    return cb(err);
               });
            }, (err) => {
                if (err) {
                    return reject(err);
                }
                return resolve();
            });
        });
      return lastIndex;
    }

    /**
     * Adds the specified file in a document series
     * @param {DataContext | HttpContext} context - The current context
     * @param {string} file - The full path of a file
     * @param {DocumentNumberSeriesItem} attributes A set of attributes that is going to be attached in document
     */
    async add(context, file, attributes) {
        return await new Promise((resolve, reject) => {
            let finalResult;
            context.db.executeInTransaction( callback => {
                if (attributes.contentType == null) {
                    return reject(new TypeError('Content type cannot be empty at this context.'))
                }
                /**
                 * @type PrivateContentService
                 */
                const svc = context.getApplication().getService( function PrivateContentService() {
                });
                // throw error if PrivateContentService is null
                if (svc == null) {
                    return callback('Content service may not be empty at this context.');
                }
                // copy item
                const newItem = Object.assign({
                    additionalType: 'DocumentNumberSeriesItem'
                }, attributes);
                return context.unattended((cb)=> {
                    // move file to content
                    return svc.copyFrom(context,  path.resolve(process.cwd(), file), newItem, err => {
                            if (err) {
                                TraceUtils.error(err);
                                return cb(new Error('An internal server error occurred while moving content.'));
                            }
                            return cb();
                        });
                }, err => {
                    if (err) {
                        return callback(err);
                    }
                    /**
                     * @type {DataModel}
                     */
                    const model = context.model(DocumentNumberSeriesItem);
                    // and customize it in order to disable base model (Attachment)
                    // do migrate
                    return model.migrate( err => {
                        if (err) {
                            return callback(err);
                        }
                        // load primary key
                        model.getPrimaryKey();
                        // override and destroy base model
                        model.base = function() {
                            return null;
                        }
                        // forcibly insert item
                        return model.where('id').equal(newItem.id).silent().count().then((exists) => {
                            if (exists) {
                                return Promise.resolve();
                            }
                            return model.insert(newItem);
                        }).then(() => {
                            return svc.resolveUrl(context, newItem, function(err, url) {
                                if (err) {
                                    return callback(err);
                                }
                                // format result
                                finalResult = Object.assign({
                                }, newItem, {
                                    url: url
                                });
                                return callback();
                            });
                        }).catch((err) => {
                            return callback(err);
                        });
                    });
                });

            }, (err)=> {
                if (err) {
                    return reject(err);
                }
                return resolve(finalResult);
            });
        });
    }

    /**
     * Replaces the specified file in a document series
     * @param {DataContext|*} context - The current context
     * @param {string} file - The full path of a file
     * @param {DocumentNumberSeriesItem} attributes A set of attributes that is going to be attached in document
     */
    async replace(context, file, attributes) {
        return await new Promise((resolve, reject) => {
            let finalResult;
            context.db.executeInTransaction( callback => {
                if (attributes.contentType == null) {
                    return reject(new TypeError('Content type cannot be empty at this context.'))
                }
                /**
                 * @type PrivateContentService
                 */
                const svc = context.getApplication().getService( function PrivateContentService() {
                });
                // throw error if PrivateContentService is null
                if (svc == null) {
                    return callback('Content service may not be empty at this context.');
                }
                // copy item
                const saveItem = Object.assign({
                    additionalType: 'DocumentNumberSeriesItem'
                }, attributes);
                return context.unattended((cb)=> {
                    // move file to content
                    return svc.copyFrom(context,  path.resolve(process.cwd(), file), saveItem, err => {
                        if (err) {
                            TraceUtils.error(err);
                            return cb(new Error('An internal server error occurred while moving content.'));
                        }
                        return cb();
                    });
                }, err => {
                    if (err) {
                        return callback(err);
                    }
                    // get model
                    const model = context.model(DocumentNumberSeriesItem);
                    // and customize it in order to disable base model (Attachment)
                    // do migrate
                    return model.migrate( err => {
                        if (err) {
                            return callback(err);
                        }
                        // load primary key
                        model.getPrimaryKey();
                        // override and destroy base model
                        model.base = function() {
                            return null;
                        }
                        // save item
                        return model.save(saveItem).then(() => {
                            return svc.resolveUrl(context, saveItem, function(err, url) {
                                if (err) {
                                    return callback(err);
                                }
                                // format result
                                finalResult = Object.assign({
                                }, saveItem, {
                                    url: url
                                });
                                // and return
                                return callback();
                            });
                        }).catch( (err) => {
                            return callback(err);
                        });
                    });
                });

            }, (err)=> {
                if (err) {
                    return reject(err);
                }
                return resolve(finalResult);
            });
        });
    }
}

export {
    DefaultDocumentNumberService
}
