import {ApplicationService, IApplication} from '@themost/common';
import {DataContext} from '@themost/data';
import DocumentNumberSeries from './models/DocumentNumberSeries';
import DocumentNumberSeriesItem from './models/DocumentNumberSeriesItem';
export declare abstract class DocumentNumberService extends ApplicationService  {
    constructor(app: IApplication);

    abstract next(context: DataContext, documentSeries: DocumentNumberSeries, extraAttributes?: any): Promise<any>;

    abstract add(context: DataContext, file: string, item: DocumentNumberSeriesItem): Promise<any>;

    abstract replace(context: DataContext, file: string, item: DocumentNumberSeriesItem): Promise<any>;

    addFrom(context: DataContext, data: Blob | Buffer, item: DocumentNumberSeriesItem): Promise<any>;

    replaceFrom(context: DataContext, data: Blob | Buffer, item: DocumentNumberSeriesItem): Promise<any>;
    
    getContentType(extname: string): string;
}